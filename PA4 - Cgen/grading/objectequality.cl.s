# start of generated code
	.data
	.align	2
	.globl	class_nameTab
	.globl	Main_protObj
	.globl	Int_protObj
	.globl	String_protObj
	.globl	bool_const0
	.globl	bool_const1
	.globl	_int_tag
	.globl	_bool_tag
	.globl	_string_tag
_int_tag:
	.word	2
_bool_tag:
	.word	3
_string_tag:
	.word	4
	.globl	_MemMgr_INITIALIZER
_MemMgr_INITIALIZER:
	.word	_NoGC_Init
	.globl	_MemMgr_COLLECTOR
_MemMgr_COLLECTOR:
	.word	_NoGC_Collect
	.globl	_MemMgr_TEST
_MemMgr_TEST:
	.word	0
	.word	-1
str_const13:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const1
	.byte	0	
	.align	2
	.word	-1
str_const12:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const3
	.ascii	"Main"
	.byte	0	
	.align	2
	.word	-1
str_const11:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const4
	.ascii	"B"
	.byte	0	
	.align	2
	.word	-1
str_const10:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const4
	.ascii	"A"
	.byte	0	
	.align	2
	.word	-1
str_const9:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const5
	.ascii	"String"
	.byte	0	
	.align	2
	.word	-1
str_const8:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const3
	.ascii	"Bool"
	.byte	0	
	.align	2
	.word	-1
str_const7:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const2
	.ascii	"Int"
	.byte	0	
	.align	2
	.word	-1
str_const6:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const6
	.ascii	"IO"
	.byte	0	
	.align	2
	.word	-1
str_const5:
	.word	4
	.word	6
	.word	String_dispTab
	.word	int_const5
	.ascii	"Object"
	.byte	0	
	.align	2
	.word	-1
str_const4:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const7
	.ascii	"_prim_slot"
	.byte	0	
	.align	2
	.word	-1
str_const3:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const8
	.ascii	"SELF_TYPE"
	.byte	0	
	.align	2
	.word	-1
str_const2:
	.word	4
	.word	7
	.word	String_dispTab
	.word	int_const8
	.ascii	"_no_class"
	.byte	0	
	.align	2
	.word	-1
str_const1:
	.word	4
	.word	8
	.word	String_dispTab
	.word	int_const9
	.ascii	"<basic class>"
	.byte	0	
	.align	2
	.word	-1
str_const0:
	.word	4
	.word	9
	.word	String_dispTab
	.word	int_const10
	.ascii	"./objectequality.cl"
	.byte	0	
	.align	2
	.word	-1
int_const10:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	19
	.word	-1
int_const9:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	13
	.word	-1
int_const8:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	9
	.word	-1
int_const7:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	10
	.word	-1
int_const6:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	2
	.word	-1
int_const5:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	6
	.word	-1
int_const4:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	1
	.word	-1
int_const3:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	4
	.word	-1
int_const2:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	3
	.word	-1
int_const1:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	0
	.word	-1
int_const0:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	5
	.word	-1
bool_const0:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	0
	.word	-1
bool_const1:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	1
class_nameTab:
	.word	str_const5
	.word	str_const6
	.word	str_const7
	.word	str_const8
	.word	str_const9
	.word	str_const10
	.word	str_const11
	.word	str_const12
class_objTab:
	.word	Object_protObj
	.word	Object_init
	.word	IO_protObj
	.word	IO_init
	.word	Int_protObj
	.word	Int_init
	.word	Bool_protObj
	.word	Bool_init
	.word	String_protObj
	.word	String_init
	.word	A_protObj
	.word	A_init
	.word	B_protObj
	.word	B_init
	.word	Main_protObj
	.word	Main_init
Object_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
IO_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	IO.out_string
	.word	IO.out_int
	.word	IO.in_string
	.word	IO.in_int
Int_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
Bool_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
String_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	String.length
	.word	String.concat
	.word	String.substr
A_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	A.foo
B_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	A.foo
Main_dispTab:
	.word	Object.abort
	.word	Object.type_name
	.word	Object.copy
	.word	Main.main
	.word	-1
Object_protObj:
	.word	0
	.word	3
	.word	Object_dispTab
	.word	-1
IO_protObj:
	.word	1
	.word	3
	.word	IO_dispTab
	.word	-1
Int_protObj:
	.word	2
	.word	4
	.word	Int_dispTab
	.word	0
	.word	-1
Bool_protObj:
	.word	3
	.word	4
	.word	Bool_dispTab
	.word	0
	.word	-1
String_protObj:
	.word	4
	.word	5
	.word	String_dispTab
	.word	int_const1
	.word	0
	.word	-1
A_protObj:
	.word	5
	.word	4
	.word	A_dispTab
	.word	int_const1
	.word	-1
B_protObj:
	.word	6
	.word	3
	.word	B_dispTab
	.word	-1
Main_protObj:
	.word	7
	.word	3
	.word	Main_dispTab
	.globl	heap_start
heap_start:
	.word	0
	.text
	.globl	Main_init
	.globl	Int_init
	.globl	String_init
	.globl	Bool_init
	.globl	Main.main
#Object
Object_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#IO
IO_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#Int
Int_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#Bool
Bool_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#String
String_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#A
A_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	#int_const
	la	$a0 int_const0
	#int_const_end
	sw	$a0 12($s0)
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#B
B_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	A_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
#Main
Main_init:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	jal	Object_init
	move	$a0 $s0
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra
	#formal
		#name:y
		#type_decl:Int
		#formal_end
A.foo:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	#block
	#assign
	#object: y
	#[y]
	lw	$a0 12($fp)
	#object_end
	sw	$a0 12($s0)
	#assign_end
	#object: self
	move	$a0 $s0
	#object_end
	#block_end
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 16
	jr	$ra
Main.main:
	addiu	$sp $sp -12
	sw	$fp 12($sp)
	sw	$s0 8($sp)
	sw	$ra 4($sp)
	addiu	$fp $sp 4
	move	$s0 $a0
	#block
	#let
		#identifier:x
		#type_decl:B
		#init
	#new:B
	la	$a0 B_protObj
	jal	Object.copy
	jal B_init
	#new_end
	sw	$a0 -4($fp)
		#init_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
		#body
	#block
	#if
	#pred
	#eq
	#object: x
	lw	$a0 -4($fp)
	#object_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#object: x
	lw	$a0 -4($fp)
	#object_end
	lw	$t1 4($sp)
	move	$t2 $a0
	addiu	$sp $sp 4
	la	$a0 bool_const1
	beq	$t1 $t2 label2
	la	$a1 bool_const0
	jal	equality_test
	#eq_end
label2:
	lw	$t1 12($a0)
	beqz	$t1 label0
	#then
	#int_const
	la	$a0 int_const1
	#int_const_end
	b	label1
	#else
label0:

	#dispatch:abort
		#pushing args
		#args pushed
	#e0
	#object: self
	move	$a0 $s0
	#object_end
	#e0
	bne	$a0 $zero label3
	la	$a0 str_const0
	li	$t1 1
	jal	_dispatch_abort
label3:
	lw	$t1 8($a0)
	#name_:abort
	#[abort, type_name, copy, Mainmain]
	#index:0
	lw	$t1 0($t1)
	jalr	$t1
	#dispatch_end

label1:
	#if_end
	#if
	#pred
	#eq
	#object: x
	lw	$a0 -4($fp)
	#object_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#new:B
	la	$a0 B_protObj
	jal	Object.copy
	jal B_init
	#new_end
	lw	$t1 4($sp)
	move	$t2 $a0
	addiu	$sp $sp 4
	la	$a0 bool_const1
	beq	$t1 $t2 label6
	la	$a1 bool_const0
	jal	equality_test
	#eq_end
label6:
	lw	$t1 12($a0)
	beqz	$t1 label4
	#then

	#dispatch:abort
		#pushing args
		#args pushed
	#e0
	#object: self
	move	$a0 $s0
	#object_end
	#e0
	bne	$a0 $zero label7
	la	$a0 str_const0
	li	$t1 1
	jal	_dispatch_abort
label7:
	lw	$t1 8($a0)
	#name_:abort
	#[abort, type_name, copy, Mainmain]
	#index:0
	lw	$t1 0($t1)
	jalr	$t1
	#dispatch_end

	b	label5
	#else
label4:
	#int_const
	la	$a0 int_const1
	#int_const_end
label5:
	#if_end
	#if
	#pred
	#eq
	#new:A
	la	$a0 A_protObj
	jal	Object.copy
	jal A_init
	#new_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#new:A
	la	$a0 A_protObj
	jal	Object.copy
	jal A_init
	#new_end
	lw	$t1 4($sp)
	move	$t2 $a0
	addiu	$sp $sp 4
	la	$a0 bool_const1
	beq	$t1 $t2 label10
	la	$a1 bool_const0
	jal	equality_test
	#eq_end
label10:
	lw	$t1 12($a0)
	beqz	$t1 label8
	#then

	#dispatch:abort
		#pushing args
		#args pushed
	#e0
	#object: self
	move	$a0 $s0
	#object_end
	#e0
	bne	$a0 $zero label11
	la	$a0 str_const0
	li	$t1 1
	jal	_dispatch_abort
label11:
	lw	$t1 8($a0)
	#name_:abort
	#[abort, type_name, copy, Mainmain]
	#index:0
	lw	$t1 0($t1)
	jalr	$t1
	#dispatch_end

	b	label9
	#else
label8:
	#int_const
	la	$a0 int_const1
	#int_const_end
label9:
	#if_end
	#let
		#identifier:y
		#type_decl:A
		#init
	#object: x
	lw	$a0 -4($fp)
	#object_end
	sw	$a0 -8($fp)
		#init_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
		#body
	#block
	#if
	#pred
	#eq
	#object: y
	lw	$a0 -8($fp)
	#object_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#object: x
	lw	$a0 -4($fp)
	#object_end
	lw	$t1 4($sp)
	move	$t2 $a0
	addiu	$sp $sp 4
	la	$a0 bool_const1
	beq	$t1 $t2 label14
	la	$a1 bool_const0
	jal	equality_test
	#eq_end
label14:
	lw	$t1 12($a0)
	beqz	$t1 label12
	#then
	#int_const
	la	$a0 int_const1
	#int_const_end
	b	label13
	#else
label12:

	#dispatch:abort
		#pushing args
		#args pushed
	#e0
	#object: self
	move	$a0 $s0
	#object_end
	#e0
	bne	$a0 $zero label15
	la	$a0 str_const0
	li	$t1 1
	jal	_dispatch_abort
label15:
	lw	$t1 8($a0)
	#name_:abort
	#[abort, type_name, copy, Mainmain]
	#index:0
	lw	$t1 0($t1)
	jalr	$t1
	#dispatch_end

label13:
	#if_end
	#if
	#pred
	#eq

	#dispatch:foo
		#pushing args
	#int_const
	la	$a0 int_const2
	#int_const_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
		#args pushed
	#e0
	#object: y
	lw	$a0 -8($fp)
	#object_end
	#e0
	bne	$a0 $zero label18
	la	$a0 str_const0
	li	$t1 1
	jal	_dispatch_abort
label18:
	lw	$t1 8($a0)
	#name_:Afoo
	#[abort, type_name, copy, Afoo]
	#index:3
	lw	$t1 12($t1)
	jalr	$t1
	#dispatch_end

	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#object: x
	lw	$a0 -4($fp)
	#object_end
	lw	$t1 4($sp)
	move	$t2 $a0
	addiu	$sp $sp 4
	la	$a0 bool_const1
	beq	$t1 $t2 label19
	la	$a1 bool_const0
	jal	equality_test
	#eq_end
label19:
	lw	$t1 12($a0)
	beqz	$t1 label16
	#then
	#int_const
	la	$a0 int_const1
	#int_const_end
	b	label17
	#else
label16:

	#dispatch:abort
		#pushing args
		#args pushed
	#e0
	#object: self
	move	$a0 $s0
	#object_end
	#e0
	bne	$a0 $zero label20
	la	$a0 str_const0
	li	$t1 1
	jal	_dispatch_abort
label20:
	lw	$t1 8($a0)
	#name_:abort
	#[abort, type_name, copy, Mainmain]
	#index:0
	lw	$t1 0($t1)
	jalr	$t1
	#dispatch_end

label17:
	#if_end
	#block_end
	addiu	$sp $sp 4
	#let_end
	#block_end
	addiu	$sp $sp 4
	#let_end
	#let
		#identifier:x
		#type_decl:A
	move	$a0 $zero
	sw	$a0 0($sp)
	addiu	$sp $sp -4
		#body
	#let
		#identifier:y
		#type_decl:B
	move	$a0 $zero
	sw	$a0 0($sp)
	addiu	$sp $sp -4
		#body
	#if
	#pred
	#eq
	#object: x
	lw	$a0 -4($fp)
	#object_end
	sw	$a0 0($sp)
	addiu	$sp $sp -4
	#object: y
	lw	$a0 -8($fp)
	#object_end
	lw	$t1 4($sp)
	move	$t2 $a0
	addiu	$sp $sp 4
	la	$a0 bool_const1
	beq	$t1 $t2 label23
	la	$a1 bool_const0
	jal	equality_test
	#eq_end
label23:
	lw	$t1 12($a0)
	beqz	$t1 label21
	#then
	#int_const
	la	$a0 int_const1
	#int_const_end
	b	label22
	#else
label21:

	#dispatch:abort
		#pushing args
		#args pushed
	#e0
	#object: self
	move	$a0 $s0
	#object_end
	#e0
	bne	$a0 $zero label24
	la	$a0 str_const0
	li	$t1 1
	jal	_dispatch_abort
label24:
	lw	$t1 8($a0)
	#name_:abort
	#[abort, type_name, copy, Mainmain]
	#index:0
	lw	$t1 0($t1)
	jalr	$t1
	#dispatch_end

label22:
	#if_end
	addiu	$sp $sp 4
	#let_end
	addiu	$sp $sp 4
	#let_end
	#block_end
	lw	$fp 12($sp)
	lw	$s0 8($sp)
	lw	$ra 4($sp)
	addiu	$sp $sp 12
	jr	$ra

# end of generated code
