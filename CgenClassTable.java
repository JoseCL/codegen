/*
Copyright (c) 2000 The Regents of the University of California.
All rights reserved.

Permission to use, copy, modify, and distribute this software for any
purpose, without fee, and without written agreement is hereby granted,
provided that the above copyright notice and the following two
paragraphs appear in all copies of this software.

IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF
CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION TO
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
*/

// This is a project skeleton file

import java.io.PrintStream;
import java.util.Vector;
import java.util.Enumeration;
import java.util.*;

/** This class is used for representing the inheritance tree during code
    generation. You will need to fill in some of its methods and
    potentially extend it in other useful ways. */
class CgenClassTable extends SymbolTable {

    /** All classes in the program, represented as CgenNode */
    private Vector nds;

    //guardamos la lista Classes
    private Classes classes;

    /** This is the stream to which assembly instructions are output */
    private PrintStream str;

    private int stringclasstag;
    private int intclasstag;
    private int boolclasstag;

    //Variables auxiliares de cool-tree.java
    private Hashtable<AbstractSymbol,Integer> classnametable = new Hashtable<AbstractSymbol,Integer>();
    private Hashtable<Class_,Integer> classtagtable = new Hashtable<Class_,Integer>();
    public Hashtable<AbstractSymbol,Integer> methodtable = new Hashtable<AbstractSymbol,Integer>();
    public Hashtable<AbstractSymbol,ArrayList<String>> disp_tables = new Hashtable<AbstractSymbol,ArrayList<String>>();
    public ArrayList<String> methods = new ArrayList<String>();
    public ArrayList<String> print = new ArrayList<String>();
    public Hashtable<AbstractSymbol,Integer> attrstable = new Hashtable<AbstractSymbol,Integer>();
    public ArrayList<AbstractSymbol> variables = new ArrayList<AbstractSymbol>();
    private int labelnumber = 0;
    private Class_ claseactual;
    private int method_offset = 0;
    public int formalslength=0;

    // The following methods emit code for constants and global
    // declarations.

    /** Emits code to start the .data segment and to
     * declare the global names.
     * */
    private void codeGlobalData() {
	// The following global names must be defined first.

	str.print("\t.data\n" + CgenSupport.ALIGN);
	str.println(CgenSupport.GLOBAL + CgenSupport.CLASSNAMETAB);
	str.print(CgenSupport.GLOBAL); 
	CgenSupport.emitProtObjRef(TreeConstants.Main, str);
	str.println("");
	str.print(CgenSupport.GLOBAL); 
	CgenSupport.emitProtObjRef(TreeConstants.Int, str);
	str.println("");
	str.print(CgenSupport.GLOBAL); 
	CgenSupport.emitProtObjRef(TreeConstants.Str, str);
	str.println("");
	str.print(CgenSupport.GLOBAL); 
	BoolConst.falsebool.codeRef(str);
	str.println("");
	str.print(CgenSupport.GLOBAL); 
	BoolConst.truebool.codeRef(str);
	str.println("");
	str.println(CgenSupport.GLOBAL + CgenSupport.INTTAG);
	str.println(CgenSupport.GLOBAL + CgenSupport.BOOLTAG);
	str.println(CgenSupport.GLOBAL + CgenSupport.STRINGTAG);

	// We also need to know the tag of the Int, String, and Bool classes
	// during code generation.

	str.println(CgenSupport.INTTAG + CgenSupport.LABEL 
		    + CgenSupport.WORD + intclasstag);
	str.println(CgenSupport.BOOLTAG + CgenSupport.LABEL 
		    + CgenSupport.WORD + boolclasstag);
	str.println(CgenSupport.STRINGTAG + CgenSupport.LABEL 
		    + CgenSupport.WORD + stringclasstag);

    }

    /** Emits code to start the .text segment and to
     * declare the global names.
     * */
    private void codeGlobalText() {
	str.println(CgenSupport.GLOBAL + CgenSupport.HEAP_START);
	str.print(CgenSupport.HEAP_START + CgenSupport.LABEL);
	str.println(CgenSupport.WORD + 0);
	str.println("\t.text");
	str.print(CgenSupport.GLOBAL);
	CgenSupport.emitInitRef(TreeConstants.Main, str);
	str.println("");
	str.print(CgenSupport.GLOBAL);
	CgenSupport.emitInitRef(TreeConstants.Int, str);
	str.println("");
	str.print(CgenSupport.GLOBAL);
	CgenSupport.emitInitRef(TreeConstants.Str, str);
	str.println("");
	str.print(CgenSupport.GLOBAL);
	CgenSupport.emitInitRef(TreeConstants.Bool, str);
	str.println("");
	str.print(CgenSupport.GLOBAL);
	CgenSupport.emitMethodRef(TreeConstants.Main, TreeConstants.main_meth, str);
	str.println("");
    }

    /** Emits code definitions for boolean constants. */
    private void codeBools(int classtag) {
	BoolConst.falsebool.codeDef(classtag, str);
	BoolConst.truebool.codeDef(classtag, str);
    }

    /** Generates GC choice constants (pointers to GC functions) */
    private void codeSelectGc() {
	str.println(CgenSupport.GLOBAL + "_MemMgr_INITIALIZER");
	str.println("_MemMgr_INITIALIZER:");
	str.println(CgenSupport.WORD 
		    + CgenSupport.gcInitNames[Flags.cgen_Memmgr]);

	str.println(CgenSupport.GLOBAL + "_MemMgr_COLLECTOR");
	str.println("_MemMgr_COLLECTOR:");
	str.println(CgenSupport.WORD 
		    + CgenSupport.gcCollectNames[Flags.cgen_Memmgr]);

	str.println(CgenSupport.GLOBAL + "_MemMgr_TEST");
	str.println("_MemMgr_TEST:");
	str.println(CgenSupport.WORD 
		    + ((Flags.cgen_Memmgr_Test == Flags.GC_TEST) ? "1" : "0"));
    }

    /** Emits code to reserve space for and initialize all of the
     * constants.  Class names should have been added to the string
     * table (in the supplied code, is is done during the construction
     * of the inheritance graph), and code for emitting string constants
     * as a side effect adds the string's length to the integer table.
     * The constants are emmitted by running through the stringtable and
     * inttable and producing code for each entry. */
    private void codeConstants() {
	// Add constants that are required by the code generator.
	AbstractTable.stringtable.addString("");
	AbstractTable.inttable.addString("0");

	AbstractTable.stringtable.codeStringTable(stringclasstag, str);
	AbstractTable.inttable.codeStringTable(intclasstag, str);
	codeBools(boolclasstag);
    }


    /** Creates data structures representing basic Cool classes (Object,
     * IO, Int, Bool, String).  Please note: as is this method does not
     * do anything useful; you will need to edit it to make if do what
     * you want.
     * */
    private void installBasicClasses() {
	AbstractSymbol filename 
	    = AbstractTable.stringtable.addString("<basic class>");
	
	// A few special class names are installed in the lookup table
	// but not the class list.  Thus, these classes exist, but are
	// not part of the inheritance hierarchy.  No_class serves as
	// the parent of Object and the other special classes.
	// SELF_TYPE is the self class; it cannot be redefined or
	// inherited.  prim_slot is a class known to the code generator.

	addId(TreeConstants.No_class,
	      new CgenNode(new class_(0,
				      TreeConstants.No_class,
				      TreeConstants.No_class,
				      new Features(0),
				      filename),
			   CgenNode.Basic, this));

	addId(TreeConstants.SELF_TYPE,
	      new CgenNode(new class_(0,
				      TreeConstants.SELF_TYPE,
				      TreeConstants.No_class,
				      new Features(0),
				      filename),
			   CgenNode.Basic, this));
	
	addId(TreeConstants.prim_slot,
	      new CgenNode(new class_(0,
				      TreeConstants.prim_slot,
				      TreeConstants.No_class,
				      new Features(0),
				      filename),
			   CgenNode.Basic, this));

	// The Object class has no parent class. Its methods are
	//        cool_abort() : Object    aborts the program
	//        type_name() : Str        returns a string representation 
	//                                 of class name
	//        copy() : SELF_TYPE       returns a copy of the object

	class_ Object_class = 
	    new class_(0, 
		       TreeConstants.Object_, 
		       TreeConstants.No_class,
		       new Features(0)
			   .appendElement(new method(0, 
					      TreeConstants.cool_abort, 
					      new Formals(0), 
					      TreeConstants.Object_, 
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.type_name,
					      new Formals(0),
					      TreeConstants.Str,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.copy,
					      new Formals(0),
					      TreeConstants.SELF_TYPE,
					      new no_expr(0))),
		       filename);

	installClass(new CgenNode(Object_class, CgenNode.Basic, this));
	
	// The IO class inherits from Object. Its methods are
	//        out_string(Str) : SELF_TYPE  writes a string to the output
	//        out_int(Int) : SELF_TYPE      "    an int    "  "     "
	//        in_string() : Str            reads a string from the input
	//        in_int() : Int                "   an int     "  "     "

	class_ IO_class = 
	    new class_(0,
		       TreeConstants.IO,
		       TreeConstants.Object_,
		       new Features(0)
			   .appendElement(new method(0,
					      TreeConstants.out_string,
					      new Formals(0)
						  .appendElement(new formal(0,
								     TreeConstants.arg,
								     TreeConstants.Str)),
					      TreeConstants.SELF_TYPE,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.out_int,
					      new Formals(0)
						  .appendElement(new formal(0,
								     TreeConstants.arg,
								     TreeConstants.Int)),
					      TreeConstants.SELF_TYPE,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.in_string,
					      new Formals(0),
					      TreeConstants.Str,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.in_int,
					      new Formals(0),
					      TreeConstants.Int,
					      new no_expr(0))),
		       filename);

	installClass(new CgenNode(IO_class, CgenNode.Basic, this));

	// The Int class has no methods and only a single attribute, the
	// "val" for the integer.

	class_ Int_class = 
	    new class_(0,
		       TreeConstants.Int,
		       TreeConstants.Object_,
		       new Features(0)
			   .appendElement(new attr(0,
					    TreeConstants.val,
					    TreeConstants.prim_slot,
					    new no_expr(0))),
		       filename);

	installClass(new CgenNode(Int_class, CgenNode.Basic, this));

	// Bool also has only the "val" slot.
	class_ Bool_class = 
	    new class_(0,
		       TreeConstants.Bool,
		       TreeConstants.Object_,
		       new Features(0)
			   .appendElement(new attr(0,
					    TreeConstants.val,
					    TreeConstants.prim_slot,
					    new no_expr(0))),
		       filename);

	installClass(new CgenNode(Bool_class, CgenNode.Basic, this));

	// The class Str has a number of slots and operations:
	//       val                              the length of the string
	//       str_field                        the string itself
	//       length() : Int                   returns length of the string
	//       concat(arg: Str) : Str           performs string concatenation
	//       substr(arg: Int, arg2: Int): Str substring selection

	class_ Str_class =
	    new class_(0,
		       TreeConstants.Str,
		       TreeConstants.Object_,
		       new Features(0)
			   .appendElement(new attr(0,
					    TreeConstants.val,
					    TreeConstants.Int,
					    new no_expr(0)))
			   .appendElement(new attr(0,
					    TreeConstants.str_field,
					    TreeConstants.prim_slot,
					    new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.length,
					      new Formals(0),
					      TreeConstants.Int,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.concat,
					      new Formals(0)
						  .appendElement(new formal(0,
								     TreeConstants.arg, 
								     TreeConstants.Str)),
					      TreeConstants.Str,
					      new no_expr(0)))
			   .appendElement(new method(0,
					      TreeConstants.substr,
					      new Formals(0)
						  .appendElement(new formal(0,
								     TreeConstants.arg,
								     TreeConstants.Int))
						  .appendElement(new formal(0,
								     TreeConstants.arg2,
								     TreeConstants.Int)),
					      TreeConstants.Str,
					      new no_expr(0))),
		       filename);

	installClass(new CgenNode(Str_class, CgenNode.Basic, this));
    }
	
    // The following creates an inheritance graph from
    // a list of classes.  The graph is implemented as
    // a tree of `CgenNode', and class names are placed
    // in the base class symbol table.
    
    private void installClass(CgenNode nd) {
	AbstractSymbol name = nd.getName();
	if (probe(name) != null) return;
	nds.addElement(nd);
	addId(name, nd);
    }

    private void installClasses(Classes cs) {
        for (Enumeration e = cs.getElements(); e.hasMoreElements(); ) {
	    installClass(new CgenNode((Class_)e.nextElement(), 
				       CgenNode.NotBasic, this));
        }
    }

    private void buildInheritanceTree() {
	for (Enumeration e = nds.elements(); e.hasMoreElements(); ) {
	    setRelations((CgenNode)e.nextElement());
	}
    }

    private void setRelations(CgenNode nd) {
	CgenNode parent = (CgenNode)probe(nd.getParent());
	nd.setParentNd(parent);
	parent.addChild(nd);
    }

    /** Constructs a new class table and invokes the code generator */
    public CgenClassTable(Classes cls, PrintStream str) {
	nds = new Vector();
	classes = cls;

	this.str = str;

	stringclasstag = 4 /* Change to your String class tag here */;
	intclasstag =    2 /* Change to your Int class tag here */;
	boolclasstag =   3 /* Change to your Bool class tag here */;

	enterScope();
	if (Flags.cgen_debug) System.out.println("Building CgenClassTable");
	
	installBasicClasses();
	installClasses(cls);
	buildInheritanceTree();

	code();

	exitScope();
    }

    /** This method is the meat of the code generator.  It is to be
        filled in programming assignment 5 */
    public void code() {
	if (Flags.cgen_debug) System.out.println("coding global data");
	codeGlobalData();

	if (Flags.cgen_debug) System.out.println("choosing gc");
	codeSelectGc();

	if (Flags.cgen_debug) System.out.println("coding constants");
	codeConstants();

		//class_nameTab
		class_nameTab();
		//class_objTab
		class_objTab();
		//dispatch tables
		dispatch_tables();
		//prototype objects
		prototype_objects();

	if (Flags.cgen_debug) System.out.println("coding global text");
	codeGlobalText();

		//object initializer
		object_init();
		//class methods
		class_methods();

    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    //metodo para codear class_nameTab
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    public void class_nameTab(){
    	str.println("class_nameTab:");
    	int cont = 0;
    	for (Enumeration e = nds.elements(); e.hasMoreElements(); ){
    		Class_ clase = (Class_)e.nextElement();
    		StringSymbol s = (StringSymbol)AbstractTable.stringtable.lookup(clase.getName().getString());
    		str.print("\t.word\t");
    		s.codeRef(str);
    		str.println("");
    		classnametable.put(clase.getName(),cont);
    		cont++;
    	}
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    //metodo para codear class_objTab
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    public void class_objTab(){
    	str.println("class_objTab:");
    	for (Enumeration e = nds.elements(); e.hasMoreElements(); ){
    		//Obtenemos cada clase 
	    	Class_ clase = (Class_)e.nextElement();
	    	str.println("	.word	"+clase.getName()+"_protObj");
	    	str.println("	.word	"+clase.getName()+"_init");	
    	}
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    //metodo para crear prototype objects
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    public void prototype_objects(){   
    	int classtag = 0;
    	for (Enumeration e = nds.elements(); e.hasMoreElements(); ) {
    		//Obtenemos cada clase (object)
	    	Class_ clase = (Class_)e.nextElement();

	    	//imprimimos el -1
	    	str.println("	.word	-1"); 	    	
	    	//Imprimimos el label del prototype object	    	
	    	CgenSupport.emitProtObjRef(clase.getName(),str);
	    	str.println(":");

	    	//Imprimimos el class_tag
	    	//StringSymbol strsym = (StringSymbol)AbstractTable.stringtable.lookup(clase.getName().getString());
	    	str.println("	.word	"+classtag);
	    	//Imprimimos el object size
	    	int size = object_size(clase,0);
	    	str.println("	.word	"+size);	
	    	//Imprimimos el pointer a la info del dispatch
	    	str.println("	.word	"+clase.getName()+"_dispTab");
	    	//Imprimimos los attrs
	    	print_attrs(clase);	

	    	//Guardo el tag de cada clase en una tabla
	    	classtagtable.put(clase,classtag);
	    	classtag++;   
        }
    }

    //funcion para imprimir los attrs de una clase
    public void print_attrs(Class_ c){
    	int offset = 0;
    	//Obtenemos los features de la clase
    	Features features = c.getFeatures();
    	for (Enumeration e = features.getElements(); e.hasMoreElements(); ){
    		//Obtenemos cada feature
	    	Feature feature = (Feature)e.nextElement();
	    	if (feature instanceof attr){
	    		attr attrib = (attr)feature; //objeto tipo attr
	    		AbstractSymbol name = attrib.getName();
	    		AbstractSymbol type = attrib.getType();
	    		
	    		attrstable.put(name,offset);
	    		offset++;

	    		//Si el attrib es un Int
	    		if (type == TreeConstants.Int){
	    			str.print("	.word	");
	    			IntSymbol intsym = (IntSymbol)AbstractTable.inttable.lookup("0");
	    			intsym.codeRef(str);
	    			str.println("");
	    		//Si el attrib es un string
	    		} else if (type == TreeConstants.Str){
	    			str.print("	.word	");
	    			StringSymbol strsym = (StringSymbol)AbstractTable.stringtable.lookup("");
	    			strsym.codeRef(str);
	    			str.println("");
	    		//Si el attrib es un bool
	    		} else if (type == TreeConstants.Bool){	    			
	    			BoolConst falseval = new BoolConst(false);
	    			str.print("\t.word\t");
	    			falseval.codeRef(str);
	    			str.println("");
	    		//Si el attrib es cualquier otro objeto
	    		} else {
	    			//void segun el cool manual (?)
	    			str.println("	.word	0"); //Cuando compilo con coolc pone 0 aqui, entonces supongo que esta bien 
	    		}
	    	}
    	}
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    //metodo para crear dispatch tables
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    public void dispatch_tables(){
    	for (Enumeration e = nds.elements(); e.hasMoreElements(); ) {
    		//Obtenemos cada clase (object)
	    	Class_ clase = (Class_)e.nextElement();	    	    	
	    	//Imprimimos el label disptable    	
	    	CgenSupport.emitDispTableRef(clase.getName(),str);
	    	str.println(":");
	    	method_offset = 0;
	    	methods = new ArrayList<String>();
	    	print = new ArrayList<String>();

	    	//Creamos las tablas en si
	    	dispatch_tables(clase);

	    	//Imprimimos las tablas
	    	for (int i=0;i<print.size();i++){
	    		str.println(print.get(i));
	    	}

	    	disp_tables.put(clase.getName(),methods);
    	}    	
    }

    public void dispatch_tables(Class_ c){
    	if (c.getName() != TreeConstants.Object_){
    		//LLamamos recursivamente a la funcion pero con la clase parent
    		Class_ parent = getClass(c.getParent());
    		dispatch_tables(parent);
    	}
    	//Obtenemos los features de la clase
    	String nombreclase = c.getName().getString();
    	Features features = c.getFeatures();
    	for (Enumeration e1 = features.getElements(); e1.hasMoreElements(); ){
			//Obtenemos cada feature
	   		Feature feature = (Feature)e1.nextElement();
	   		if (feature instanceof method){

	   			//str.println("	.word	"+c.getName()+"."+((method)feature).getName());
	   			method metodo = (method)feature;
	   			String nombre = metodo.getName().getString();
	   			//Agregamos el metodo junto con la clase a la que pertenece
	   			if (c.getName() != TreeConstants.Object_ && c.getName() != TreeConstants.IO && c.getName() != TreeConstants.Str){
	   				int posicion = -1;
	   				//verificamos si el metodo esta siendo sobreescrito o no
	   				for (int i=0;i<methods.size();i++){
	   					//System.out.println("O:"+methods.get(i)+"\tN:"+nombre+"\ttf?"+methods.get(i).contains(nombre));
	   					if (methods.get(i).contains(nombre)) {
	   						posicion = i;
	   					}
	   				}

	   				if (posicion != -1){
	   					methods.add(posicion,nombreclase+nombre);
	   					methods.remove(posicion+1);
	   					print.add(posicion,("\t.word\t"+nombreclase+"."+nombre));
	   					print.remove(posicion+1);
	   				} else {
	   					methods.add(nombreclase+nombre);
	   					print.add("\t.word\t"+nombreclase+"."+nombre);
	   				}  				
	   				
	   			//Si es un metodo de una clase basica, lo agregamos solo asi
	   			} else {
	   				methods.add(nombre);
	   				print.add("\t.word\t"+nombreclase+"."+nombre);
	   			}
	   			
	   		}
    	}

    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    //metodo para inicializar objetos
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    public void object_init(){
    	int offset = 3;
    	for (Enumeration e = nds.elements(); e.hasMoreElements(); ) {
    		//Obtenemos cada clase (object)
	    	Class_ clase = (Class_)e.nextElement();
	    	claseactual = clase;	
	    	AbstractSymbol object = clase.getName();
	    	str.println("#"+object);
	    	str.println(object+"_init:");        
	        str.println("	addiu	$sp $sp -12");
	        str.println("	sw	$fp 12($sp)");
	        str.println("	sw	$s0 8($sp)");
	        str.println("	sw	$ra 4($sp)");
	        str.println("	addiu	$fp $sp 4");
	        str.println("	move	$s0 $a0");

	        //Cool-tour dice:
	        //The code generator must define Main_init. Main_init should execute all initialization code of
			//Main’s parent classes and finally execute the initializations of attributes in Main (if there are any).

			//Sin embargo, coolc realiza lo mismo con las demas clases, asi que mejor decidi ponerlo 
        	if (object != TreeConstants.Object_){
        		AbstractSymbol parent = clase.getParent();
        		CgenSupport.emitJal((parent+"_init"),str);
           	}

           	//inicializacion de attrs
           	Features features = clase.getFeatures();
           	int strcont = 1, intcont = 1, idcont = 1, attrib_cont = 0;
           	for (Enumeration e1 = features.getElements(); e1.hasMoreElements(); ){
				//Obtenemos cada feature
		   		Feature feature = (Feature)e1.nextElement();
		   		if (feature instanceof attr && clase.getName() != TreeConstants.Object_ && clase.getName() != TreeConstants.IO && clase.getName() != TreeConstants.Str && clase.getName() != TreeConstants.Int && clase.getName() != TreeConstants.Bool){
		   			attr attrib = (attr)feature; //objeto tipo attr
		    		AbstractSymbol name = attrib.getName();
		    		AbstractSymbol type = attrib.getType();
		    		Expression init = attrib.getInit();
		    		if (!(init instanceof no_expr)){
		    			try{
		    				init.code(str,this);
		    				CgenSupport.emitStore(CgenSupport.ACC,3+attrib_cont,CgenSupport.SELF,str);
		    				attrib_cont++;
		    			} catch (Exception exc){
		    			}		    			
		    		}		
		   		}
		   	}

	        str.println("	move	$a0 $s0");
	        str.println("	lw	$fp 12($sp)");
	        str.println("	lw	$s0 8($sp)");
	        str.println("	lw	$ra 4($sp)");
	        str.println("	addiu	$sp $sp 12");
	        str.println("	jr	$ra");
    	}
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    //metodo para codear los metodos de los objetos
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    public void class_methods(){
    	for (Enumeration e = nds.elements(); e.hasMoreElements(); ) {
    		//Obtenemos cada clase (object)
	    	Class_ clase = (Class_)e.nextElement();
	    	claseactual = clase;
	    	if (clase.getName() != TreeConstants.Object_ && clase.getName() != TreeConstants.IO && clase.getName() != TreeConstants.Str){
		    	//Obtenemos la lista de features 
		    	Features features = clase.getFeatures();
		    	for (Enumeration e1 = features.getElements(); e1.hasMoreElements(); ){
					//Obtenemos cada feature
		   			Feature feature = (Feature)e1.nextElement();
		   			if (feature instanceof method){
		   				method metodo = (method)feature;
		   				//Calculando la longtud de formals
		   				formalslength = 0;
				        Formals formals = metodo.getFormals();
		   				for (Enumeration e2 = formals.getElements();e2.hasMoreElements(); ){
				        	((Formal)e2.nextElement()).code(str,this);
				        	formalslength++;
				        }

		   				str.println(clase.getName()+"."+metodo.getName()+":");

		   				str.println("	addiu	$sp $sp -12");
				        str.println("	sw	$fp 12($sp)");
				        str.println("	sw	$s0 8($sp)");
				        str.println("	sw	$ra 4($sp)");
				        str.println("	addiu	$fp $sp 4");
				        str.println("	move	$s0 $a0");

				        //Aqui empezamos a utilizar cool-tree.java
				        //Le aplicamos code(str,this) a la expr de cada metodo
				        metodo.getExpression().code(str,this);

				        str.println("	lw	$fp 12($sp)");
				        str.println("	lw	$s0 8($sp)");
				        str.println("	lw	$ra 4($sp)");		 
				        CgenSupport.emitAddiu(CgenSupport.SP,CgenSupport.SP,12+formalslength*4,str);
				        str.println("	jr	$ra");		

				        for (Enumeration e3 = formals.getElements();e3.hasMoreElements(); ){
				        	formal f = (formal)e3.nextElement();
				        	remove_var(f.getName());
				        }		        
		   			}
	    		}	    		
    		}
    	}
    }





    //funcion para encontrar el size del objeto recursivamente 
    public int object_size(Class_ c,int n){
    	int size = n;
    	if (c.getName() != TreeConstants.Object_){
    		//Obtenemos los features de la clase
    		Features features = c.getFeatures();
    		for (Enumeration e = features.getElements(); e.hasMoreElements(); ){
    			//Obtenemos cada feature
	    		Feature feature = (Feature)e.nextElement();
	    		if (feature instanceof attr)n++; //sumamos +1 cada vez que encontramos un attr
    		}
    		//LLamamos recursivamente a la funcion pero con la clase parent y un nuevo n
    		Class_ parent = getClass(c.getParent());
    		object_size(parent,n);
    	}
    	return (n+3); //size +3 por el tamaño default de object
    }

    //funcion para devolver la clase
    public Class_ getClass(AbstractSymbol classname){
    	for (Enumeration e = nds.elements(); e.hasMoreElements(); ){
    		//Obtenemos cada clase 
	    	Class_ clase = (Class_)e.nextElement();
	    	if (classname == clase.getName()) return clase;
    	}
    	return null;
    }

    //Funcion que devuelve el numero de label actual y aumenta al mismo tiempo ese numero
    public int label_number(){
    	labelnumber++;
    	return (labelnumber-1);
    }

    //Funcion que devuelve la clase actual
    public Class_ clase_actual(){
    	return claseactual;
    }

    public int method_offset(AbstractSymbol clase,AbstractSymbol name){
    	String s = "";
    	ArrayList<String> listametodos = disp_tables.get(clase);
    	//verificamos si el metodo pertenece a las clases basicas
    	if (listametodos.indexOf(name.getString()) != -1) {
    		s = name.getString();
    	//Verificamos si el metodo esta definido en la clase actual
    	} else if (listametodos.indexOf(clase.getString()+name.getString()) != -1){
    		s = clase.getString()+name.getString();
    	//Si lo anterior falla, buscamos el metodo en los parent
    	} else {
    		s = clase.getString()+name.getString();
    		str.println("\t#name_:"+s); 
    		while (listametodos.indexOf(s) == -1){
    			clase = getClass(clase).getParent();
    			s = clase.getString()+name.getString();
    		}
    	}

    	str.println("\t#name_:"+s);    	
    	str.println("\t#"+listametodos.toString());
    	str.println("\t#index:"+listametodos.indexOf( s ));
		return listametodos.indexOf(s);
    }

    public int attr_offset(AbstractSymbol name){
    	if (attrstable.get(name) != null){
    		return attrstable.get(name);	
    	} else {
    		return -1;
    	}    	
    }

    public int classname_offset(AbstractSymbol name){
    	return classnametable.get(name);
    }

    public int formals_length(){
    	return formalslength;
    }

    //Metodos para guardar, remover y buscar variables 
    public int var_offset(AbstractSymbol name){
    	return variables.indexOf(name);
    }

    public int formal_offset(AbstractSymbol name){
    	return (variables.size() - (variables.indexOf(name)+1))+3;
    }

    public void add_var(AbstractSymbol name){
    	//Añadimos la variable si no existe
    	if (variables.indexOf(name) == -1){
    		variables.add(name);
    	//Reemplazamos la variable si ya existe
    	} else {
    		int pos = variables.indexOf(name);
    		variables.add(pos,name);
    		variables.remove(pos+1);
    	}
    	
    }

    public void remove_var(AbstractSymbol name){
    	variables.remove(name);
    }

    //funcion que me devuelve el tag de una clase
    public int getClassTag(AbstractSymbol c){
    	Class_ clase = getClass(c);
    	return classtagtable.get(clase);
    }

    /** Gets the root of the inheritance tree */
    public CgenNode root() {
	return (CgenNode)probe(TreeConstants.Object_);
    }
}
			  
    
